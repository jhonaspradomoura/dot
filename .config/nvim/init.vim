set nocompatible

let mapleader = ','

set guicursor=n-v-c:block,i-ci:ver30,r:hor20          		
set termguicolors				
set showmatch			    	" Show matching brackets
set number			        	" Show line numbers
set formatoptions=jcroql	   	" Continue comment markers in new lines
set noexpandtab			    	" Don't insert spaces when TAB is pressed
set tabstop=4			    	" Number of spaces used on TAB

set autoread 					" When a file is changed outside of the terminal automatically updates it
set splitbelow			        " Horizontal split below current
set splitright			        " Vertical split to right of current

set nostartofline		        " Do not jump to first character with page commands

set textwidth=0               " Maximum text width before wrapping
set wrap
set autoindent                  " Use same indenting on new lines
set smartindent                 " Smart autoindenting on new lines

set ignorecase                  " Search ignoring case
set smartcase                   " Keep case when searching with *
set incsearch                   " Incrementally search for matches as you type
set hlsearch                    " Highlight search results
set wrapscan                    " Searches wrap the end of the file

set backspace=indent,eol,start  " Intuitive backspacing in insert mode
set switchbuf=useopen,usetab    " Jump to the first open window in any tab
set showfulltag                 " Show tag and tidy search in completion
set complete=.                  " No wins, buffs, tags, include scanning
set completeopt+=menuone         " Show menu even for one item
set completeopt+=noselect       " Do not select a match in the menu

runtime! macros/matchit.vim 	" Runs the version of matchit that ships with vim
	
inoremap jj <ESC>

if !exists('g:vscode')
		"----------------------------------------------------------------
		"                  COC CONFIGURATIONS
		"----------------------------------------------------------------
		" TextEdit might fail if hidden is not set.
		set hidden

		" Some servers have issues with backup files, see #649.
		set nobackup
		set nowritebackup

		" Give more space for displaying messages.
		set cmdheight=2

		" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
		" delays and poor user experience.
		set updatetime=300

		" Don't pass messages to |ins-completion-menu|.
		set shortmess+=c

		" Always show the signcolumn, otherwise it would shift the text each time diagnostics appear/become resolved.  set signcolumn=yes Use tab for trigger completion with characters ahead and navigate.  NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin before putting this into your config. Expendable or Jumpable line is for coc-snippets plugin 
		inoremap <silent><expr> <TAB>
			  \ pumvisible() ? "\<C-n>" :
			  \ <SID>check_back_space() ? "\<TAB>" :
			  \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
			  \ coc#refresh()
		inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

		function! s:check_back_space() abort
		  let col = col('.') - 1
		  return !col || getline('.')[col - 1]  =~# '\s'
		endfunction

		" Use <c-space> to trigger completion.
		inoremap <silent><expr> <c-space> coc#refresh()

		" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
		" position. Coc only does snippet and additional edit on confirm.
		if exists('*complete_info')
		  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
		else
		  imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
		endif

		" Use `[g` and `]g` to navigate diagnostics
		nmap <silent> [g <Plug>(coc-diagnostic-prev)
		nmap <silent> ]g <Plug>(coc-diagnostic-next)

		" GoTo code navigation.
		nmap <silent> gd <Plug>(coc-definition)
		nmap <silent> gy <Plug>(coc-type-definition)
		nmap <silent> gi <Plug>(coc-implementation)
		nmap <silent> gr <Plug>(coc-references)

		" Use K to show documentation in preview window.
		nnoremap <silent> <leader>d :call <SID>show_documentation()<CR>

		function! s:show_documentation()
		  if (index(['vim','help'], &filetype) >= 0)
			execute 'h '.expand('<cword>')
		  else
			call CocAction('doHover')
		  endif
		endfunction

		" Highlight the symbol and its references when holding the cursor.
		autocmd CursorHold * silent call CocActionAsync('highlight')

		" Symbol renaming.
		nmap <leader>rn <Plug>(coc-rename)

		" Formatting selected code.
		xmap <leader>f  <Plug>(coc-format-selected)
		nmap <leader>f  <Plug>(coc-format-selected)

		augroup mygroup
		  autocmd!
		  " Setup formatexpr specified filetype(s).
		  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
		  " Update signature help on jump placeholder.
		  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
		augroup end

		" Applying codeAction to the selected region.
		" Example: `<leader>aap` for current paragraph
		xmap <leader>a  <Plug>(coc-codeaction-selected)
		nmap <leader>a  <Plug>(coc-codeaction-selected)

		" Remap keys for applying codeAction to the current line.
		nmap <leader>ac  <Plug>(coc-codeaction)
		" Apply AutoFix to problem on the current line.
		nmap <leader>qf  <Plug>(coc-fix-current)

		" Introduce function text object
		" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
		xmap if <Plug>(coc-funcobj-i)
		xmap af <Plug>(coc-funcobj-a)
		omap if <Plug>(coc-funcobj-i)
		omap af <Plug>(coc-funcobj-a)

		" Use <TAB> for selections ranges.
		" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
		" coc-tsserver, coc-python are the examples of servers that support it.
		nmap <silent> <TAB> <Plug>(coc-range-select)
		xmap <silent> <TAB> <Plug>(coc-range-select)

		" Add `:Format` command to format current buffer.
		command! -nargs=0 Format :call CocAction('format')

		" Add `:Fold` command to fold current buffer.
		command! -nargs=? Fold :call     CocAction('fold', <f-args>)

		" Add `:OR` command for organize imports of the current buffer.
		command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

		" Add (Neo)Vim's native statusline support.
		" NOTE: Please see `:h coc-status` for integrations with external plugins that
		" provide custom statusline: lightline.vim, vim-airline.
		set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

		" Mappings using CoCList:
		" Show all diagnostics.
		nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
		" Manage extensions.
		nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
		" Show commands.
		nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
		" Find symbol of current document.
		nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
		" Search workspace symbols.
		nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
		" Do default action for next item.
		nnoremap <silent> <space>j  :<C-u>CocNext<CR>
		" Do default action for previous item.
		nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
		" Resume latest coc list.
		nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

		"              CoC Yank
		nnoremap <silent> <space>y  :<C-u>CocList -A --normal yank<cr>
				   
		"              CoC Snippets
		 " Use <C-l> for trigger snippet expand.
		imap <C-l> <Plug>(coc-snippets-expand)

		" Use <C-j> for select text for visual placeholder of snippet.
		vmap <C-j> <Plug>(coc-snippets-select)

		" Use <C-j> for both expand and jump (make expand higher priority.)
		imap <C-j> <Plug>(coc-snippets-expand-jump)            
endif
""---------------------------------------------------------
"" 					SYNTASTIC
""---------------------------------------------------------

"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatusLineFlag()}
"set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 0
"let g:syntastic_check_on_wq = 1

"---------------------------------------------------------

"---------------------------------------------------------
"                   QUICK-SCOPE
"---------------------------------------------------------

let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

augroup qs_colors
  autocmd!
  autocmd ColorScheme * highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
  autocmd ColorScheme * highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
augroup END
if exists('g:vscode')
		highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
		highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
endif

"---------------------------------------------------------

"---------------------------------------------------------
"                   CamelCaseMotion
"---------------------------------------------------------
let g:camelcasemotion_key = '<leader>'
map <silent> w <Plug>CamelCaseMotion_w
map <silent> b <Plug>CamelCaseMotion_b
map <silent> e <Plug>CamelCaseMotion_e
map <silent> ge <Plug>CamelCaseMotion_ge
sunmap w
sunmap b
sunmap e
sunmap ge
omap <silent> iw <Plug>CamelCaseMotion_iw
xmap <silent> iw <Plug>CamelCaseMotion_iw
omap <silent> ib <Plug>CamelCaseMotion_ib
xmap <silent> ib <Plug>CamelCaseMotion_ib
omap <silent> ie <Plug>CamelCaseMotion_ie
xmap <silent> ie <Plug>CamelCaseMotion_ie
imap <silent> <S-Left> <C-o><Plug>CamelCaseMotion_b
imap <silent> <S-Right> <C-o><Plug>CamelCaseMotion_w
"---------------------------------------------------------

if !&scrolloff
        set scrolloff=3	        " Show next 3 lines while scrolling.
endif
if !&sidescrolloff
		set sidescrolloff=5	    " Show next 5 columns while side-scrolling.
endif

set rnu							" Show line number as relative numbers
" set nornu

" Use deoplete.
" let g:deoplete#enable_at_startup = 1

source $HOME/.config/nvim/plugins.vim
" Remember folded groups on exit
augroup remember_folds
		autocmd!
		autocmd BufWinLeave * mkview
		autocmd BufWinEnter * silent! loadview
augroup END

" Change  to CtrlP
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

lua require'colorizer'.setup()


syntax on
if !exists('g:vscode')
		colorscheme dracula
endif

" Make comments italic
highlight Comment cterm=italic gui=italic guifg=#90a0ff

" Go to the first non-blank character of a line
noremap 0 ^
" Just in case you need to go to the very beginning of a line
noremap ^ 0

