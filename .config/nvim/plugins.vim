if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
		curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
		autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin()
		if !exists('g:vscode')
				Plug 'neoclide/coc.nvim', {'branch': 'release'}
				Plug 'scrooloose/nerdtree'
				Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }}
				Plug 'ctrlpvim/ctrlp.vim'
				Plug 'airblade/vim-gitgutter'
		endif
"		Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' },
"		Plug 'arakashic/chromatica.nvim',
		Plug 'norcalli/nvim-colorizer.lua', {'as': 'colorizer'}
		Plug 'vim-airline/vim-airline'
		"Plug 'vim-syntastic/syntastic'
		Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
		Plug 'junegunn/fzf.vim'
		Plug 'itchyny/lightline.vim'
		Plug 'dracula/vim', { 'as': 'dracula' }
		Plug 'tpope/vim-surround'
		Plug 'editorconfig/editorconfig-vim'
		Plug 'terryma/vim-multiple-cursors'
		Plug 'jiangmiao/auto-pairs'
		Plug 'tpope/vim-unimpaired'
		Plug 'wellle/targets.vim'
		Plug 'preservim/nerdcommenter'
		Plug 'justinmk/vim-sneak'
		Plug 'nathanaelkane/vim-indent-guides'
		Plug 'terryma/vim-expand-region'
		"Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
		"Plug 'mboughaba/i3config.vim'
		Plug 'taketwo/vim-ros'
		Plug 'jackguo380/vim-lsp-cxx-highlight'
		Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
		Plug 'kovetskiy/sxhkd-vim'
		Plug 'unblevable/quick-scope'
		Plug 'bkad/CamelCaseMotion'
		"Plug 'davidhalter/jedi-vim'
call plug#end()

