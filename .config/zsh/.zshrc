# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


if [ -z "$ZSH_COMPDUMP" ]; then
		ZSH_COMPDUMP="${ZDOTDIR:-$HOME}/.zcompdump-${SHORT_HOST}-${ZSH_VERSION}"
fi

setopt AUTO_CD
setopt AUTO_PUSHD
setopt PUSHD_IGNORE_DUPS
setopt ALWAYS_TO_END
setopt AUTO_LIST
#setopt AUTO_MENU
setopt AUTO_NAME_DIRS
setopt AUTO_PARAM_SLASH
setopt COMPLETE_ALIASES
setopt GLOB_COMPLETE
setopt MENU_COMPLETE
setopt EXTENDED_GLOB
setopt GLOB_STAR_SHORT
setopt HIST_SUBST_PATTERN
setopt NULL_GLOB
setopt APPEND_HISTORY
setopt EXTENDED_HISTORY
setopt HIST_IGNORE_SPACE
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_NO_STORE
setopt HIST_REDUCE_BLANKS
setopt SHARE_HISTORY
setopt ALIASES
setopt HASH_LIST_ALL
setopt INTERACTIVE_COMMENTS
setopt PATH_DIRS
setopt PATH_SCRIPT
setopt monitor
setopt LONG_LIST_JOBS
setopt interactive
setopt zle
setopt PROMPTSUBST

source $ZDOTDIR/.completion
#---------------------
# Load antigen plugins
export ADOTDIR=$ZDOTDIR/antigen
export ANTIGEN_AUTO_CONFIG=false
typeset -a ANTIGEN_CHECK_FILES=($ZDOTDIR $ZDOTDIR/antigen)
source $ZDOTDIR/antigen.zsh

# Load oh-my-zsh repository
antigen use oh-my-zsh

antigen bundle command-not-found
antigen bundle zdharma/fast-syntax-highlighting
antigen bundle qoomon/zsh-lazyload
antigen bundle sudo
antigen bundle pacman
antigen bundle yay
antigen bundle zsh-users/zsh-autosuggestions

antigen bundle docker
antigen bundle docker-compose
# Sensible settings for zsh
#antigen bundle okuramasafumi/zsh-sensible

# You should use alias hinting
antigen bundle "MichaelAquilina/zsh-you-should-use"

# IntelliSense like auto completion for zsh
#antigen bundle marlonrichert/zsh-autocomplete

# Vi mode plugins
#antigen bundle zsh-vi-more/vi-motions
#antigen bundle sinetoami/vi-mode

# Add FZF completion for git commands
antigen bundle hschne/fzf-git

# git aliases
antigen bundle mdumitru/git-aliases

# Vim like recording macros
antigen bundle cal2195/q

# autoupdate antigen
antigen bundle unixorn/autoupdate-antigen.zshplugin

# fzf completion for zsh
antigen bundle Aloxaf/fzf-tab

# Cycle through directories
antigen bundle michaelxmcbride/zsh-dircycle

# Create a cheatsheet with cs command
antigen bundle 0b10/cheatsheet

# Auto notifies when a job is finished
antigen bundle MichaelAquilina/zsh-auto-notify

# Create .in and .out files to be run when entering or exiting a folder
antigen bundle zpm-zsh/autoenv

# Auto-closes, deletes or skips over any matching delimiters
antigen bundle hlissner/zsh-autopair

# Use gitit to open git folder on web
antigen bundle peterhurford/git-it-on.zsh

antigen bundle skywind3000/z.lua

#--- THEMES ---
antigen theme romkatv/powerlevel10k


antigen apply
#------------------

fpath+=$ZDOTDIR/.zfunc

# Enable vi mode
#bindkey -v 

# Use CTRL-Z to go back to foreground process
fancy-ctrl-z () {
  if [[ $#BUFFER -eq 0 ]]; then
    BUFFER="fg"
    zle accept-line
  else
    zle push-input
    zle clear-screen
  fi
}
zle -N fancy-ctrl-z
bindkey '^Z' fancy-ctrl-z


export ZSH_AUTOSUGGEST_USEASYNC
export EDITOR=nvim
source $ZDOTDIR/.aliases
source $HOME/.profile
#source $ZDOTDIR/.default_local

source $HOME/.local/scripts/setup_funcs.sh

# Inicializa FZF
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
export FZF_DEFAULT_COMMAND='fd --type f'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"


# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f $ZDOTDIR/.p10k.zsh ]] || source $ZDOTDIR/.p10k.zsh

# Temporary fix for systemctl completion
_systemctl_unit_state()
{
		typeset -gA _sys_unit_state
		_sys_unit_state=($(__systemctl list-unit-files "$PREFIX*" | awk '{print $1, $2}'))
}

autoload -U compinit && compinit

# Use up and down arrow to complete from command history
bindkey '^[OA' history-beginning-search-backward
bindkey '^[OB' history-beginning-search-forward

source $ZDOTDIR/.fzftab
source $ZDOTDIR/.local
