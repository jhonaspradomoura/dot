#!/usr/bin/zsh

declare -A profiles

set_profile()
{
		prof=$(echo $profile_list | rofi -dmenu "What profile do you wish to set?") 
		eval $profiles[$prof]
}

agroeyes()
{
		echo "pyenv activate agroeyes && source $HOME/dev/agroeyes/Fontes/Ros/devel/setup.zsh"
}

tcc()
{
		echo "pyenv activate mpr && source $HOME/dev/mpr/devel/setup.zsh"
}

local_prof()
{
		echo "pyenv deactivate && source /opt/ros/melodic/setup.zsh"
}

profiles[agroeyes]="agroeyes"
profiles[tcc]="tcc"
profiles[local]="local_prof"

profile_list=$(echo ${(@k)profiles} | sed "s/\s/\n/g")

