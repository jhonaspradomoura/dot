#!/usr/bin/zsh

declare -A proxy
declare -A proxy_profile

set_proxy()
{
		proxy_choice=$(echo $proxy_list | rofi -dmenu -p "Select your proxy configuration")
		set_proxy_profile $proxy[$proxy_choice]
}

kyros()
{
		echo "192.168.0.1:3128\njhonasp@kyros.com.br"
}

casa()
{
		echo "''\njhonaspradomoura@gmail.com" 
}

proxy_profile[kyros]="kyros"
proxy_profile[casa]="casa"

proxy_profile_list=$(echo ${(@k)proxy_profile} | sed "s/\s/\n/g")

git()
{
		value=$(eval $proxy_profile[$1])
		ip=$(echo $value | cut -d $'\n' -f 1)
		email=$(echo $value | cut -d $'\n' -f 2)
		echo "git config --global http.proxy $ip;git config --global https.proxy $ip;git config --global user.email $email"
}

tty()
{
		value=$(eval $proxy_profile[$1])
		ip=$(echo $value | cut -d $'\n' -f 1)
		[[ -z $ip ]] && echo "export {http,https}_proxy=http://$ip" || echo "unset {http,https,ftp}_proxy"
}

set_proxy_profile()
{
		proxy_profile_choice=$(echo $proxy_profile_list | rofi -dmenu -p "Select the proxy profile")
		eval $1 $proxy_profile_choice
}

proxy[git]='git'
proxy[tty]='tty'

proxy_list=$(echo ${(@k)proxy} | sed "s/\s/\n/g")
