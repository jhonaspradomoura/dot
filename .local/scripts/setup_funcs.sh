#!/usr/bin/zsh

current=${0%/*}
ISR=$'\n' files=($(ls $current | sed "/\(\.sh\)/ d"))

for file in $files; do
		[[ -f /usr/local/bin/$file ]] || ( echo "Linking personal scripts to /usr/local/bin" ; sudo -A ln -sr $file /usr/local/bin/)

done;

