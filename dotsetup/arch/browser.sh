#!/usr/bin/bash

browser_options=(1 "vivaldi"\
				 2 "brave"\
				 3 "firefox"\
				 4 "chromium")

browser_cmd=$(dialog --menu "Which browser do you want to install?" 22 76 16)

browser_choice=$("${cmd[@]}" "${browser_options[@]}" 2>&1 > /dev/tty)
case $browser_choice in
		1)
				[[ $(command -v vivaldi-stable ) ]] && dialog --title "Already installed!" --msgbox "Vivaldi is already installed" 05 40 || \
						(sudo yay -S vivaldi vivaldi-widevine &&\
				[[ $(command -v vivaldi-stable ) ]] && dialog --title "Successfully installed!" --msgbox "Vivaldi is installed" 05 40 || dialog --title "Failure" --msgbox "Vivaldi package was not installed!" 05 40)
				;;
		2)
				[[ $(command -v brave ) ]] && dialog --title "Already installed!" --msgbox "Brave is already installed" 05 40 || \
						(sudo pacman -S brave && \
				[[ $(command -v brave ) ]] && dialog --title "Successfully installed!" --msgbox "Brave is installed" 05 40 || dialog --title "Failure" --msgbox "Brave package was not installed!" 05 40)
						;;
		3)
				[[ $(command -v firefox ) ]] && dialog --title "Already installed!" --msgbox "Firefox is already installed" 05 40 || \
						(sudo pacman -S firefox &&\
				[[ $(command -v firefox ) ]] && dialog --title "Successfully installed!" --msgbox "Firefox is already installed" 05 40 || dialog --title "Failure" --msgbox "Firefox package was not installed!" 05 40)
				;;
		4)
				[[ $(command -v chromium ) ]] && dialog --title "Already installed!" --msgbox "Chromium is already installed" 05 40 || \
						(sudo pacman -S chromium && \
				[[ $(command -v chromium ) ]] && dialog --title "Successfully installed!" --msgbox "Chromium is already installed" 05 40 || dialog --title "Failure" --msgbox "Chromium package was not installed!" 05 40)
				;;
esac

