#!/usr/bin/bash

editor_options=(1 "neovim"\
				2 "vim")

editor_cmd=(dialog --menu "Which editor do you want to install?" 22 76 16)

editor_choice=$("${cmd[@]}" "${editor_options[@]}" 2>&1 > /dev/tty)
case $editor_choice in
		1)
				clear
				[[ $(command -v nvim) ]] && dialog --title "Already installed!" --msgbox "neovim is already installed!" 05 40 || \ 
						(sudo pacman -S neovim && \
						([[ $(command -v nvim) ]] &&\ 
						dialog --title "Success" --msgbox "neovim package installed!" 05 40 || \
						dialog --title "Failure" --msgbox "neovim package was not installed!" 05 40))
				;;
		2)
				clear
				[[ $(command -v /usr/bin/vim) ]] && dialog --title "Already installed" --msgbox "vim is already installed" 05 40 || 
				(sudo pacman -S vim && \
				[[ $(command -v /usr/bin/vim ) ]] && \
				dialog --title "Successfully installed!" --msgbox "vim is already installed" 05 40 || \
				dialog --title "Failure" --msgbox "vim package was not installed!" 05 40)
				;;

esac
