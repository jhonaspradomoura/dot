#!/usr/bin/bash

echo "Installing dialog and yay packages.."
[[ $(which dialog) ]] || sudo pacman -S dialog
[[ $(which yay) ]] || sudo pacman -S yay

distro_options=(1 "Shell"\
				2 "Terminal-emulator"\
				3 "Text Editor"\
				4 "Browser"\
				5 "Window Manager"\
				6 "Miscellanea"
				)

root_dir=$(dirname "$0")
msg="Installation Options:"
cmd=(dialog --keep-window --menu "$msg" 22 76 16)

while :
do
		choice=$( "${cmd[@]}" "${distro_options[@]}" 2>&1 >/dev/tty)
		[[ "$?"="0" && -z $choice ]] && echo "breaking" && break
		case $choice in
		1) 
				source $root_dir/shell.sh
				clear;;
		2) 
				source $root_dir/terminal_emulator.sh
				clear;;
		3)
				source $root_dir/editor.sh
				clear;;
		4)
				source $root_dir/browser.sh
				clear;;
		5)
				source $root_dir/wm.sheditor
				clear;;
		6)
				dialog --title "Wrong answer.." --msgbox "Miscellanea installation is not implemented yet..." 22 76
				clear;;
		esac
done
clear
