#!/usr/bin/bash

shell_options=(1 "zsh"\
		       2 "fish")

shell_cmd=(dialog --menu "Which shell do you want to install?" 22 76 16)

shell_choice=$("${cmd[@]}" "${shell_options[@]}" 2>&1 > /dev/tty)
case $shell_choice in
		1)
				clear
				ZDOTDIR=$HOME/.config/zsh
				[[ -d "$ZDOTDIR" ]] || mkdir $ZDOTDIR
				[[ -f "$ZDOTDIR/antigen.zsh" ]] || curl -s -L git.io/antigen > $ZDOTDIR/antigen.zsh 
				[[ $(command -v zsh) ]] && dialog --title "Already installed!" --msgbox "zsh is already installed" 05 40 || \ 
				(sudo pacman -S zsh && \
				[ $(chsh -s  $(command -V zsh | awk -F'[  ]' '{print $3}')) ]] && \ 
				dialog --title "Success" --msgbox "zsh package installed!" 05 40 || dialog --title "Failure" --msgbox "zsh package was not installed" 05 40)
				;;
		2)
			dialog --title "Wrong answer.." --msgbox "Fish installation is not implemented yet..." 22 76 
esac

