#!/usr/bin/bash

emulator_options=(1 "kitty"\
				  2 "alacritty"\
				  3 "terminator")

msg="Which terminal emulator would you rather install?"
cmd=(dialog --menu "$msg" 22 76 16)

choice=$("${cmd[@]}" "${emulator_options[@]}" 2>&1 >/dev/tty)
case $choice in
		1)
				clear
				[[ $(command -v kitty) ]] && (dialog --title "Already installed" --msgbox "kitty terminal is already installed!" 05 40) || \
					( sudo pacman -S kitty && \
					dialog --title "Success" --msgbox "kitty terminal installed! You must set the environmental variable TERM to xterm-kitty" 15 40 || \
					dialog --title "Failure" --msgbox "kitty terminal was not installed!" 15 40)
				;;
		2)
				dialog --title "Wrong answer.." --msgbox "Alacritty installation is not implemented yet..." 22 76 
				;;	
		3)
				dialog --title "Wrong answer.." --msgbox "Terminator installation is not implemented yet..." 22 76 
				;;	
esac
