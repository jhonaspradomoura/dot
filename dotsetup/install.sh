#!/usr/bin/bash

[[ $(which dialog) ]] || sudo pacman -S dialog

distro_options=(1 "Arch"\
				2 "Debian")

root_dir=$(dirname "$0")
msg="Which distro are you running?"
cmd=(dialog --keep-window --clear --menu "$msg" 22 76 16)

choice=$( "${cmd[@]}" "${distro_options[@]}" 2>&1 >/dev/tty)
#clear
case $choice in
		1) 
				exec $root_dir/arch/install.sh
				;;
		2) dialog --title "Wrong answer.." --msgbox "Debian installation is not implemented yet." 22 76; clear;;

esac
